#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_socket(new QTcpSocket(this)),
    m_waitHeader(true)
{
    ui->setupUi(this);
    ui->hostEdit->setText("localhost");

    //disable sending message while connection is not established
    ui->sendMsgButton->setDisabled(true);

    //TODO: need a Disconnect button

    connect(ui->connectButton, &QPushButton::pressed, this, &MainWindow::connectPressed);
    connect(ui->sendMsgButton, &QPushButton::pressed, this, &MainWindow::sendPressed);

    connect(m_socket, &QTcpSocket::connected, this, &MainWindow::connectionReady);
    connect(m_socket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &MainWindow::onNetError);
    connect(m_socket, &QTcpSocket::readyRead, this, &MainWindow::onRead);
    connect(m_socket, &QTcpSocket::bytesWritten, this, &MainWindow::onWrite);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectPressed()
{
    if(ui->hostEdit->text().isEmpty())
    {
        QMessageBox::warning(this, "Connecting", "Empty host");
        return;
    }
    if(ui->nameEditor->text().isEmpty())
    {
        QMessageBox::warning(this, "Connecting", "Empty user name");
        return;
    }
    m_socket->connectToHost(ui->hostEdit->text(), 5656);    //TODO: read port number from input
}

void MainWindow::sendPressed()
{
    sendMessage(OWN_MESSAGE, ui->messageEditor->text().toUtf8());
    appendChatMessage(ui->nameEditor->text(), ui->messageEditor->text(), true);
    ui->messageEditor->clear();
}

void MainWindow::connectionReady()
{
    ui->chatEdit->append(QString("<b>Connected to host '%1'</b>").arg(ui->hostEdit->text()));
    sendMessage(USER_NAME, ui->nameEditor->text().toUtf8());

    //now enable sending message and disable new connection
    ui->sendMsgButton->setEnabled(true);
    ui->hostEdit->setDisabled(true);
    ui->nameEditor->setDisabled(true);
    ui->connectButton->setDisabled(true);
}

void MainWindow::onNetError(QAbstractSocket::SocketError)
{
    ui->chatEdit->append(QString("<font color=\"red\">ERROR: %1</font>").arg(m_socket->errorString()));
}

void MainWindow::onRead()
{
    while(m_socket->bytesAvailable() > 0)
    {
        if(m_waitHeader)
        {
            if(m_socket->bytesAvailable() >= (qint64)sizeof(m_header))
            {
                m_socket->read((char*)&m_header, sizeof(m_header));

                if(m_header.length > 0)
                    m_waitHeader = false; //and go to data read
                else processMessage(QByteArray());
            }
            else break;
        }
        else
        {
            if(m_socket->bytesAvailable() >= m_header.length)
            {
                QByteArray data = m_socket->read(m_header.length);
                m_waitHeader = true;
                processMessage(data);
            }
            else break;
        }
    }
}

void MainWindow::onWrite(quint64 bytes)
{
    m_outBuffer = m_outBuffer.mid(bytes);

    if(m_outBuffer.length() > 0)
        m_socket->write(m_outBuffer);
}

void MainWindow::sendMessage(quint16 type, const QByteArray &data)
{
    PacketHeader hdr;
    hdr.type = type;
    hdr.length = data.length();
    m_outBuffer.append((char*)&hdr, sizeof(hdr));
    m_outBuffer.append(data);
    //TODO: what to do if the connection is slow and the buffer becomes too large

    if(m_socket->bytesToWrite() == 0)
        m_socket->write(m_outBuffer);
}

void MainWindow::processMessage(const QByteArray &data)
{
    switch(m_header.type)
    {
    case USER_MESSAGE: {
        QList<QByteArray> list = data.split('\0');
        appendChatMessage(QString::fromUtf8(list[0]), QString::fromUtf8(list[1]), false);
        }
        break;

    default:
        break;
    }
}

void MainWindow::appendChatMessage(const QString &username, const QString &text, bool own)
{
    ui->chatEdit->append(QString("<font color=\"%3\">%1:</font> %2").
                         arg(username, text).
                         arg(own ? "red" : "blue"));
}
