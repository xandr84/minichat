#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include "../include/protocol.h"

namespace Ui {
class MainWindow;
}

class QTcpSocket;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void connectPressed();      //when Connect pressed check fields and connect to host
    void sendPressed();         //when Send button pressed send message and clear input
    void connectionReady();     //called when socket establish connection
    void onNetError(QAbstractSocket::SocketError socketError);  //socket error handler
    void appendChatMessage(const QString &username, const QString &text, bool own); //append one line to chat text box

private:
    Ui::MainWindow *ui;
    QTcpSocket *m_socket;
    bool m_waitHeader;      //what we are wait from socket - header or data
    PacketHeader m_header;  //temporary header, that read from socket
    QByteArray m_outBuffer; //buffer for outgoing messages

    void onRead();                  //called when some bytes available for read
    void onWrite(quint64 bytes);    //called when some bytes written to socket
    void processMessage(const QByteArray &data);    //process received from stream message
    void sendMessage(quint16 type, const QByteArray &data); //pack message to m_outBuffer
};

#endif // MAINWINDOW_H
