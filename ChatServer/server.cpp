#include "server.h"
#include "session.h"
#include <QDebug>

Server::Server(QObject *parent) : QObject(parent)
{
    connect(&m_server, &QTcpServer::newConnection, this, &Server::onNewClient);
}

bool Server::listen(quint16 port)
{
    qDebug() << "Start listening on port " << port;
    return m_server.listen(QHostAddress::Any, port);
}

void Server::onNewClient()
{
    while(m_server.hasPendingConnections())
    {
        Session* session = new Session(m_server.nextPendingConnection(), this);
        m_clients.append(session);
    }
}

void Server::broadcastMessage(Session *session, const QString &text)
{
    for(Session *s : m_clients)
    {
        if(s != session)
            s->sendText(session, text);
    }
}

void Server::removeClient(Session *session)
{
    m_clients.removeOne(session);
}
