#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QList>
#include <QtNetwork/QTcpServer>

class Session;

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);

    bool listen(quint16 port = 5656);

signals:

public slots:
    void onNewClient();     //process connection of new clients
    void broadcastMessage(Session *session, const QString &text);   //send message for all clients except `session`
    void removeClient(Session *session);    //remove client when it disconnect

private:
    QTcpServer m_server;
    QList<Session*> m_clients;
};

#endif // SERVER_H
