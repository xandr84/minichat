#include "session.h"
#include "server.h"

#include <QByteArray>
#include <QtNetwork/QTcpSocket>

Session::Session(QTcpSocket *socket, Server *server) :
    QObject(server), m_server(server), m_socket(socket)
{
    m_waitHeader = true;
    connect(m_socket, &QTcpSocket::readyRead, this, &Session::onRead);
    connect(m_socket, &QTcpSocket::bytesWritten, this, &Session::onWrite);
    connect(m_socket, &QTcpSocket::disconnected, this, &Session::onDisconnect);
    connect(m_socket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
        this, &Session::onError);

    qInfo() << getID() << "New connection from " << m_socket->peerAddress().toString();
}

void Session::sendText(Session *session, const QString &text)
{
    QByteArray data;
    data.append(session->m_username.toUtf8());
    data.append('\0');
    data.append(text.toUtf8());
    sendMessage(MessageType::USER_MESSAGE, data);
}

void Session::onRead()
{
    while(m_socket->bytesAvailable() > 0)
    {
        if(m_waitHeader)
        {
            if(m_socket->bytesAvailable() >= (qint64)sizeof(m_header))
            {
                m_socket->read((char*)&m_header, sizeof(m_header));

                if(m_header.length > 0)
                    m_waitHeader = false; //and go to data read
                else processMessage(QByteArray());
            }
            else break;
        }
        else
        {
            if(m_socket->bytesAvailable() >= m_header.length)
            {
                QByteArray data = m_socket->read(m_header.length);
                m_waitHeader = true;
                processMessage(data);
            }
            else break;
        }
    }
}

void Session::onWrite(quint64 bytes)
{
    m_outBuffer = m_outBuffer.mid(bytes);

    if(m_outBuffer.length() > 0)
        m_socket->write(m_outBuffer);
}

void Session::onDisconnect()
{
    m_server->removeClient(this);
    qInfo() << getID() << "Disconnect";
}

void Session::onError(QAbstractSocket::SocketError)
{
    qCritical() << getID() << m_socket->errorString();
}

void Session::processMessage(const QByteArray &data)
{
    switch(m_header.type)
    {
    case MessageType::USER_NAME:
        if(data.length() == 0)
        {
            sendMessage(MessageType::ERROR,
                        QString("Empty user name").toLatin1());
            qDebug() << getID() << "Empty user name";
        }
        else
        {
            m_username = QString::fromUtf8(data);
            qDebug() << getID() << "Receive USER_NAME" << m_username.toLocal8Bit();
        }
        break;

    case MessageType::OWN_MESSAGE:
        if(m_username.length() == 0)
        {
            sendMessage(MessageType::ERROR,
                        QString("Need user name before text sending").toLatin1());
            qDebug() << getID() << "Need user name before text sending";
        }
        else
        {
            qDebug() << getID() << "Receive OWN_MESSAGE";
            m_server->broadcastMessage(this, QString::fromUtf8(data));
        }
        break;

    default:
        sendMessage(MessageType::ERROR,
                    QString("Unknown packet type %1").arg(m_header.type).toLatin1());
        qDebug() << getID() << "Unknown packet type";
        break;
    }
}

void Session::sendMessage(quint16 type, const QByteArray &data)
{
    PacketHeader hdr;
    hdr.type = type;
    hdr.length = data.length();
    m_outBuffer.append((char*)&hdr, sizeof(hdr));
    m_outBuffer.append(data);
    qDebug() << getID() << "Send message " << type << " size " << data.length();
    //TODO: what to do if the connection is slow and the buffer becomes too large

    if(m_socket->bytesToWrite() == 0)
        m_socket->write(m_outBuffer);
}

QString Session::getID() const
{
    return QString("[%1] ").arg(reinterpret_cast<uintptr_t>(this), 8, 16);
}
