#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include <QTcpSocket>
#include "../include/protocol.h"

class Server;

class Session : public QObject
{
    Q_OBJECT
public:
    explicit Session(QTcpSocket *socket, Server *server);

signals:

public slots:    
    void sendText(Session *session, const QString &text);   //send message by user of session

private:
    Server *m_server;
    QTcpSocket *m_socket;
    QString m_username;
    bool m_waitHeader;      //what we are wait from socket - header or data
    PacketHeader m_header;  //temporary header, that read from socket
    QByteArray m_outBuffer; //buffer for outgoing messages

    void onRead();              //called when some bytes available for read
    void onWrite(quint64 bytes);//called when some bytes written to socket
    void onDisconnect();        //called when client go away
    void onError(QAbstractSocket::SocketError socketError); //socket error handler
    void processMessage(const QByteArray &data);    //process received from stream message
    void sendMessage(quint16 type, const QByteArray &data); //pack message to m_outBuffer

    QString getID() const;  //unique session ID (convert addr to hex)
};

#endif // SESSION_H
