# MiniChat #

Simple chat using Qt 5 and binary TCP protocol

Video example of work in Ubuntu in file MiniChat.wmv

![Example](MiniChat.jpg)

## Usage ##

Run ChatServer

Run ChatClient, enter Host and Username, press Connect, write message and click Send