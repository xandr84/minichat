#ifndef PROTOCOL_H
#define PROTOCOL_H

/* ******* Protocol description ********
 * Client connect to server, send USER_NAME then can send OWN_MESSAGE.
 * Server send USER_MESSAGE to all users after receive message from anybody.
 * Network packet structure:
 *   type:uint16
 *   length:uint16
 *   data:bytes
 */

enum MessageType
{
    ERROR = 0,          //text with a error info
    USER_NAME = 1,      //text with a user name
    OWN_MESSAGE = 2,    //text with a message
    USER_MESSAGE = 3    //text with a user name and message separated by \0
};

struct PacketHeader
{
    quint16 type;
    quint16 length;
};

#endif // PROTOCOL_H
